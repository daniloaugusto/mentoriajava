package org.example;

import java.time.LocalDate;
import java.util.Date;

public class Salario extends Conta{

    private Integer numeroConta;
    private LocalDate dataAbertura;

    public Salario(Integer numeroConta, LocalDate dataAbertura, Integer banco, Integer agencia) {
        super(banco, agencia);
        this.numeroConta = numeroConta;
        this.dataAbertura = dataAbertura;
    }

}
