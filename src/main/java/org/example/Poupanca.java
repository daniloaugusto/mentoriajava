package org.example;

import java.time.LocalDate;
import java.util.Date;

public class Poupanca extends Conta {

    private Integer numeroConta;
    private LocalDate dataAbertura;

    private Double saldo;

    public Poupanca(Integer numeroConta, LocalDate dataAbertura, Integer banco, Integer agencia, Double saldo) {
        super(banco, agencia);
        this.numeroConta = numeroConta;
        this.dataAbertura = dataAbertura;
        this.saldo = saldo;
    }
}
