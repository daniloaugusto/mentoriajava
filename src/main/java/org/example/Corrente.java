package org.example;

import java.time.LocalDate;
import java.util.Date;

public class Corrente extends Conta{

    private Integer numeroConta;
    private LocalDate dataAbertura;

    public Corrente(Integer numeroConta, LocalDate dataAbertura, Integer banco, Integer agencia) {
        super(banco, agencia);
        this.numeroConta = numeroConta;
        this.dataAbertura = dataAbertura;
    }

}
